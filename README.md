# Downloading

Open a terminal and navigate to where you want to have the directory for the project, and run:

'git clone https://gitlab.com/xerxespersrex/csc-205-ab-honors-project.git'

# Building

Either compile the java files on your own, or on a UNIX/Linux system navigate to the project directory in a terminal and run 'make'

# Running

Navigate to the folder where the .class files are (if you run 'make', it should be the 'build/' directory) and run:

'java HashGenerator SHA-256 /path/to/your/file'

This will generate an SHA-256 hash of the file given in the same format as the sha256sum command on GNU/Linux systems.

There are other options instead of the SHA-256 algorithm I created: namely, the built-in SHA-256, MD5, and SHA-1 algorithms can also be used. Just change the SHA-256 in the command like this:

'java HashGenerator "MD5 built-in" /path/to/your/file'

(Make sure the "MD5 built-in" is in quotes).

