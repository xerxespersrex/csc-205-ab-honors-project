//Needed to print output.
import java.io.*;

public class HashGenerator
{
	public static void main(String[] args)
	{
		if (args.length <= 1)
		{
			System.out.println("No arguments given.");

			return;
		}

		String hash = args[0];

		for (int i = 1; i < args.length; ++i)
		{
			HashStringGenerator hasher = new HashStringGenerator(hash, args[i]);

			System.out.println(hasher.toString() + "  " + args[i]);
		}

		return;
	}
}

