//SHA-2 algorithms, including SHA256, are patented in the United States
//(patent # 6,829,355) by the National Institute of Standards and
//Technology, and have been released royalty-free by the United States
//government.

//Needed for IOException.
import java.io.*;

//Needed for file io.
import java.nio.*;
import java.nio.file.*;

//Needed for MessageDigest I believe.
import java.security.*;

class HashStringGenerator
{
	String hashtype = "";
	String filename = "";
	String hash = "";

	public HashStringGenerator()
	{
		this.hashtype = "";
		this.filename = "";
		this.hash = "";
	}

	public HashStringGenerator(String type)
	{
		this.hashtype = type;
		this.filename = "";

		this.hash = "";
	}

	public HashStringGenerator(String type, String file)
	{
		this.hashtype = type;
		this.filename = file;

		this.hash = "";
	}

	public void setHashType(String type)
	{
		if (this.hashtype.compareTo(type) != 0)
		{
			this.hashtype = type;
			this.hash = "";
		}
	}

	public void setFileName(String file)
	{
		if (this.filename.compareTo(file) != 0)
		{
			this.filename = file;
			this.hash = "";
		}
	}

	private void generateHash()
	{
		if (this.hashtype.compareTo("") == 0 || this.hashtype == null ||
		    this.filename.compareTo("") == 0 || this.filename == null)
		{
			this.hash = "";

			return;
		}

		if (this.hashtype.compareTo("SHA-256") == 0)
		{
			this.generateOurSHA256();

			return;
		}

		if (this.hashtype.compareTo("SHA-256 built-in") == 0)
		{
			this.generateJavaHash("SHA-256");

			return;
		}

		if (this.hashtype.compareTo("MD5 built-in") == 0)
		{
			this.generateJavaHash("MD5");

			return;
		}

		if (this.hashtype.compareTo("SHA-1 built-in") == 0)
		{
			this.generateJavaHash("SHA-1");

			return;
		}

		return;
	}

	private void generateJavaHash(String javaHashType)
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance(javaHashType);

			byte[] hashBytes = md.digest(this.readFile());

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < hashBytes.length; ++i)
			{
				sb.append(String.format("%02x", hashBytes[i]));
			}

			this.hash = sb.toString();
		}

		catch (Exception e)
		{
			//Do nothing.
		}

		return;
	}

	private byte[] readFile() throws IOException
	{
		try
		{
			byte[] filebytes = Files.readAllBytes(Paths.get(this.filename));

			return filebytes;
		}

		catch (Exception e)
		{
			throw new IOException();
		}
	}

	private byte[] SHA256Padding(byte[] input)
	{
		try
		{
			int filesize = input.length;

			long bits = (long)(filesize) * 8L;

			long padding;

			for (padding = 0; (bits + 1 + padding + 64) % 512 != 0; ++padding)
			{
				//Do nothing.
			}

			if ((bits + 1 + padding + 64) % 8 != 0)
			{
				return null;
			}

			if ((1 + padding) % 8 != 0)
			{
				return null;
			}

			byte[] inputextended = new byte[(int)((bits + 1L + padding + 64L) / 8L)];

			int buffersize = -1;

			for (int i = 0; i < filesize; ++i)
			{
				inputextended[i] = input[i];
				buffersize = i;
			}

			++buffersize;

			inputextended[buffersize] = 0;
			inputextended[buffersize] = (byte)(inputextended[buffersize] | (1 << 7));

			++buffersize;

			for (int j = (int)(((1L + padding) / 8L) - 1L); j > 0; --j)
			{
				inputextended[buffersize] = 0;

				++buffersize;
			}

			inputextended[buffersize++] = (byte)((bits >>> 56) & 0xFF);
                	inputextended[buffersize++] = (byte)((bits >>> 48) & 0xFF);
                	inputextended[buffersize++] = (byte)((bits >>> 40) & 0xFF);
                	inputextended[buffersize++] = (byte)((bits >>> 32) & 0xFF);
                	inputextended[buffersize++] = (byte)((bits >>> 24) & 0xFF);
                	inputextended[buffersize++] = (byte)((bits >>> 16) & 0xFF);
                	inputextended[buffersize++] = (byte)((bits >>> 8) & 0xFF);
                	inputextended[buffersize++] = (byte)(bits & 0xFF);

			if (buffersize != inputextended.length)
			{
				System.out.println("New error check failed!");

				return null;
			}

			return inputextended;
		}

		catch (Exception e)
		{
			System.out.println("Caught exception");

			return null;
		}
	}

	private void generateOurSHA256()
	{
		byte[] filebytes;

		try
		{
			//Read file.
			filebytes = this.readFile();

			if (filebytes == null)
			{
				this.hash = "";

				return;
			}

			//Pad file.
			filebytes = SHA256Padding(filebytes);

			if (filebytes == null)
			{
				this.hash = "";

				return;
			}

			//Calculate SHA256

			int[] hashvalues = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};

                        int[] roundconstants = {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};

			for (int i = 0; i < filebytes.length; i += 64)
			{
				int[] words = new int[64];

				//Initialize to 0 (doesn't matter).
				for (int j = 0; j < 64; ++j)
				{
					words[j] = 0;
				}

				//Set words' actual values.
				for (int j = 0; j < 16; ++j)
				{
					int temp = 4 * j + i;
					words[j] = ((filebytes[temp] << 24) & 0xFF000000) |
						   ((filebytes[temp + 1] << 16) & 0xFF0000) |
						   ((filebytes[temp + 2] << 8) & 0xFF00) |
						   ((filebytes[temp + 3]) & 0xFF);
				}

				for (int j = 16; j < 64; ++j)
				{
					int tempone = Integer.rotateRight(words[j - 15], 7) ^ Integer.rotateRight(words[j - 15], 18) ^ (words[j - 15] >>> 3);

					int temptwo = Integer.rotateRight(words[j - 2], 17) ^ Integer.rotateRight(words[j - 2], 19) ^ (words[j - 2] >>> 10);

					words[j] = words[j - 16] + tempone + words[j - 7] + temptwo;
				}

				//Create workingvars array.
				int workingvars[] = new int[8];

				//Initialize to hash values.
				for (int j = 0; j < 8; ++j)
				{
					workingvars[j] = hashvalues[j];
				}

				//Crunch the actual working variables' values.
				for (int j = 0; j < 64; ++j)
				{
					int tempone = Integer.rotateRight(workingvars[4], 6) ^ Integer.rotateRight(workingvars[4], 11) ^ Integer.rotateRight(workingvars[4], 25);
					int temptwo = (workingvars[4] & workingvars[5]) ^ ((~workingvars[4]) & workingvars[6]);
					int tempthree = workingvars[7] + tempone + temptwo + roundconstants[j] + words[j];
					int tempfour = Integer.rotateRight(workingvars[0], 2) ^ Integer.rotateRight(workingvars[0], 13) ^ Integer.rotateRight(workingvars[0], 22);

					int tempfive = (workingvars[0] & workingvars[1]) ^ (workingvars[0] & workingvars[2]) ^ (workingvars[1] & workingvars[2]);

					int tempsix = tempfour + tempfive;

					workingvars[7] = workingvars[6];
					workingvars[6] = workingvars[5];
					workingvars[5] = workingvars[4];
					workingvars[4] = workingvars[3] + tempthree;
					workingvars[3] = workingvars[2];
					workingvars[2] = workingvars[1];
					workingvars[1] = workingvars[0];
					workingvars[0] = tempthree + tempsix;
				}

				//Add working variables to hash values.
				for (int j = 0; j < 8; ++j)
				{
					hashvalues[j] += workingvars[j];
				}
			}

			//Create string from hex hash values.
                        String hashstring = "";

                        for (int i = 0; i < hashvalues.length; ++i)
                        {
                                String temp = "00000000" + Integer.toHexString(hashvalues[i]);

                                temp = temp.substring(temp.length() - 8);

                                hashstring = hashstring + temp;
                        }

			//Save to this.hash and exit.
			this.hash = hashstring;

			return;
		}

		catch (Exception e)
		{
			this.hash = "";

			return;
		}

	}

	public String getHash()
	{
		if (this.hash.compareTo("") == 0 || this.hash == null)
		{
			this.generateHash();
		}

		return this.hash;
	}

	@Override
	public String toString()
	{
		String temphash = this.getHash();

		if (temphash == "" || temphash == null)
		{
			return "No hash generated or an error occured.";
		}

		return temphash;
	}
}
