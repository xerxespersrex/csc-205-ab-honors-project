JAVAC = javac
JAVACFLAGS = -Xlint:all
OBJDIR = build
SRCDIR = src
OBJS = $(OBJDIR)/HashStringGenerator.class $(OBJDIR)/HashGenerator.class

.PHONY: build clean

build: $(OBJS)

$(OBJS): $(OBJDIR)/
	$(JAVAC) $(JAVACFLAGS) -d $(OBJDIR) $(SRCDIR)/*.java

$(OBJDIR)/:
	mkdir -p $(OBJDIR)/

#We make absolutely certain we're not about to rm -rf /bin/,
#which, though unlikely, could happen if someone ran
#"sudo make clean" on this makefile while navigated to / .
clean:
	if [ x"$$(pwd)" != x"/" ]; then \
		if [ x"$$(pwd)" != x"//" ]; then \
			rm -rf $(OBJDIR); \
		fi \
	fi

